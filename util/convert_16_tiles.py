from PIL import Image

image = Image.open("image.png")
pixels = image.load()

out_file = open("image.bin", "wb")

for tiley in range(16):
	for tilex in range(16):
		for y in range(8):
			for x in range(8):
				pixel = pixels[x + tilex * 8, y + tiley * 8]
				out_file.write(chr(pixel))