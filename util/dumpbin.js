var fs = require('fs');

fs.stat("./image.bin", function (err, stats) {
    const fileSize = stats.size;
    const bytesToRead = fileSize;
    const position = fileSize - bytesToRead;
    fs.open("./image.bin", 'r', function (errOpen, fd) {
        fs.read(fd, Buffer.alloc(bytesToRead), 0, bytesToRead, position, function (errRead, bytesRead, buffer) {
            const bytes = [];
            for (const byte of buffer.values()) {
                bytes.push(byte);
            }

            fs.writeFileSync("../src/rom/tilerom.ts", "export const tilerom = " + JSON.stringify(bytes));
        });
    });
});