import { decimalToBinary } from "utils";

export class Counter {

    public h: boolean[];
    public v: boolean[];

    public hblank: boolean;
    public hsync: boolean;

    public vblank: boolean;
    public vsync: boolean;

    public hCount: number;
    public vCount: number;

    constructor() {
        this.hCount = 0;
        this.vCount = 0;
    }

    public clock() {
        this.hCount++;

        if (this.hCount >= 528) {
            this.hCount = 0;
            this.vCount++;
        }

        if (this.vCount >= 628) {
            this.vCount = 0;
        }

        this.hsync = (this.hCount >= 420 && this.hCount <= 484);
        this.vsync = (this.vCount >= 601 && this.vCount <= 605);

        this.hblank = (this.hCount > 400);
        this.vblank = (this.vCount > 600);

        this.h = decimalToBinary(this.hCount);
        this.v = decimalToBinary(this.vCount);
    }
}