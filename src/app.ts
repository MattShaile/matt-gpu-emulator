import { ColorDecoder } from "color-decoder";
import { count } from "console";
import { Counter } from "counter";
import { EEPROM } from "eeprom";
import { maprom } from "rom/maprom";
import { tilerom } from "rom/tilerom";
import { Screen } from "screen";
import { decimalToBinary } from "utils";

const testmap = [];
for (let y = 0; y < 128; y++) {
    for (let x = 0; x < 128; x++) {
        testmap[x + y * 128] = y % 8 + (Math.floor(y / 8) * 16);
    }
}

const counter = new Counter();
const screen = new Screen();
const mapROM = new EEPROM(maprom);
const tileROM = new EEPROM(tilerom);
const colorDecoder = new ColorDecoder();

const clockSpeed = 20000000;

let scrollX = 0;
let scrollY = 0;

let blankLatch = false;

function tick() {
    const clocksPerFrame = clockSpeed;

    for (let t = 0; t < clocksPerFrame; t++) {
        counter.clock();
        if (!counter.hblank && !counter.vblank) {

            const h = decimalToBinary(counter.hCount + scrollX);
            const v = decimalToBinary(counter.vCount + scrollY);

            mapROM.address([
                h[3],
                h[4],
                h[5],
                h[6],
                h[7],
                h[8],
                h[9],
                v[4],
                v[5],
                v[6],
                v[7],
                v[8],
                v[9]
            ]);

            const tileIndex = decimalToBinary(mapROM.output);

            tileROM.address([h[0], h[1], h[2], v[1], v[2], v[3], ...tileIndex]);
        }

        if (counter.vblank) {
            if (!blankLatch) {
                scrollX += 5;
                scrollY += 5;
            }
            blankLatch = true;
        } else {
            blankLatch = false;
        }

        const color = colorDecoder.decode(tileROM.output);

        // Screen runs at 2x clock speed
        screen.drawPixel(color);
        screen.drawPixel(color);

        if (counter.hCount === 0 && counter.vCount === 0) {
            screen.draw();
            requestAnimationFrame(() => tick());
            return;
        }
    }

    // requestAnimationFrame(() => tick());
}

requestAnimationFrame(() => tick());