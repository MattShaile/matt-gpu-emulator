import { binaryToDecimal } from "utils";

export class EEPROM {

    public output: number;

    constructor(private rom: number[]) {
    }

    public address(binary: boolean[]) {
        const decimalAddress = binaryToDecimal(binary);

        this.output = this.rom[decimalAddress];
    }
}