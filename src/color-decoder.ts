export class ColorDecoder {
    public decode(input: number) {
        const r = (input & 0b1);
        const g = (input & 0b10) >> 1;
        const b = (input & 0b100) >> 2;
        const p = (input & 0b1000) >> 3;

        let brightness = p ? 0xff : 0x88;

        let color = 0;
        color += (r * brightness) << 16;
        color += (g * brightness) << 8;
        color += (b * brightness) << 0;

        return color;
    }
}