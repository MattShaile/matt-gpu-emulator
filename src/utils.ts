export function decimalToBinary(decimal: number): boolean[] {
    const binary = [];
    for (let i = 0; i < 10; i++) {
        const mask = 1 << i;
        binary.push(!!(decimal & mask));
    }

    return binary;
}

export function binaryToDecimal(binary: boolean[]): number {
    let decimal = 0;
    for (let i = 0; i < binary.length; i++) {
        decimal += (binary[i] ? 1 : 0) << i;
    }

    return decimal;
}