export class Screen {

    private canvas: HTMLCanvasElement;
    private context: CanvasRenderingContext2D;

    private renderBuffer: ImageData;
    private width: number = 800;
    private height: number = 600;

    private x: number;
    private y: number;

    constructor() {
        this.x = this.y = 0;

        this.canvas = document.getElementById("screen") as HTMLCanvasElement;
        this.context = this.canvas.getContext("2d");

        this.renderBuffer = this.context.createImageData(this.width, this.height);
        this.canvas.width = this.width;
        this.canvas.height = this.height;

        for (var i = 0; i < this.width * this.height * 4; i += 4) {
            const color = 0;
            this.renderBuffer.data[i] = color >> 16 & 0xff;
            this.renderBuffer.data[i + 1] = color >> 8 & 0xff;
            this.renderBuffer.data[i + 2] = color & 0xff;
            this.renderBuffer.data[i + 3] = 0xff;
        }
    }

    public draw() {
        this.context.putImageData(this.renderBuffer, 0, 0);
    }

    public drawPixel(color: number) {
        if (this.x <= this.width && this.y <= this.height) {
            const n = (this.x * 4) + (this.y * this.width * 4);

            this.renderBuffer.data[n] = color >> 16 & 0xff;
            this.renderBuffer.data[n + 1] = color >> 8 & 0xff;
            this.renderBuffer.data[n + 2] = color & 0xff;
            this.renderBuffer.data[n + 3] = 0xff;

        }
        this.nextPixel();
    }

    private nextPixel() {
        this.x++;
        if (this.x >= 1056) {
            this.x = 0;
            this.y++;
        }
        if (this.y >= 628) {
            this.y = 0;
        }
    }
}